// greeter.ts
/*function greeter(person: string) {
  return `Hello ${person}!`
}

const names = 'Node Hero'

console.log(greeter(name)) 

import app from './App'

const port =  3000

app.listen(port, (err) => {
  if (err) {
    return console.log(err)
  }

  return console.log(`server is listening on ${port}`)
})

*/

import { createServer, Server } from 'http';
import * as express from 'express';
import * as socketIo from 'socket.io';


export class ChatServer {
    public static readonly PORT:number = 8080;
    private app: express.Application;
    private server: Server;
    private io: SocketIO.Server;
    private port: string | number;

    constructor() {
        this.createApp();
        this.config();
        this.createServer();
        this.sockets();
        this.listen();
    }

    private createApp(): void {
        this.app = express();
    }

    private createServer(): void {
        this.server = createServer(this.app);
    }

    private config(): void {
        this.port = process.env.PORT || ChatServer.PORT;
    }

    private sockets(): void {
        this.io = socketIo(this.server);
    }

    private listen(): void {
        this.server.listen(this.port, () => {
            console.log('Running server on port %s', this.port);
        });

        this.io.on('connect', (socket: any) => {
            console.log(process.memoryUsage());
            console.log('Connected client on port %s.', this.port);
            socket.on('message', (m : any) => {
                console.log('[server]', JSON.stringify(m));
                this.io.emit('messages', m);
            });

            socket.on('disconnect', () => {
            	 console.log(process.memoryUsage());
                console.log('Client disconnected');
            });
        });
    }

    public getApp(): express.Application {
        return this.app;
    }
}

let app = new ChatServer().getApp();

