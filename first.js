"use strict";
// greeter.ts
/*function greeter(person: string) {
  return `Hello ${person}!`
}

const names = 'Node Hero'

console.log(greeter(name))

import app from './App'

const port =  3000

app.listen(port, (err) => {
  if (err) {
    return console.log(err)
  }

  return console.log(`server is listening on ${port}`)
})

*/
exports.__esModule = true;
var http_1 = require("http");
var express = require("express");
var socketIo = require("socket.io");
var ChatServer = /** @class */ (function () {
    function ChatServer() {
        this.createApp();
        this.config();
        this.createServer();
        this.sockets();
        this.listen();
    }
    ChatServer.prototype.createApp = function () {
        this.app = express();
    };
    ChatServer.prototype.createServer = function () {
        this.server = http_1.createServer(this.app);
    };
    ChatServer.prototype.config = function () {
        this.port = process.env.PORT || ChatServer.PORT;
    };
    ChatServer.prototype.sockets = function () {
        this.io = socketIo(this.server);
    };
    ChatServer.prototype.listen = function () {
        var _this = this;
        this.server.listen(this.port, function () {
            console.log('Running server on port %s', _this.port);
        });
        this.io.on('connect', function (socket) {
            console.log(process.memoryUsage());
            console.log('Connected client on port %s.', _this.port);
            socket.on('message', function (m) {
                console.log('[server]', JSON.stringify(m));
                _this.io.emit('messages', m);
            });
            socket.on('disconnect', function () {
                console.log(process.memoryUsage());
                console.log('Client disconnected');
            });
        });
    };
    ChatServer.prototype.getApp = function () {
        return this.app;
    };
    ChatServer.PORT = 8080;
    return ChatServer;
}());
exports.ChatServer = ChatServer;
var app = new ChatServer().getApp();
