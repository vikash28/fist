/**
 * Created by thihara on 12/9/16.
 */
let express = require("express");
let webPush = require("web-push");
let atob = require('atob');
let bodyParser = require('body-parser');
let util = require('util');

let app = express();

// { endpoint: 'https://updates.push.services.mozilla.com/wpush/v2/gAAAAABbEjAZ8lSpz2rIMEYRW4otqS4BEmX27oQzZWugzjlAxOh7eQJ7CWZ-AevTOio0isz_qTaNVz2y9kZ0uKJvbYRJMj-7nP1UX7HUiV2txNm6oe5TIGf2JNoBl3oToyBNcyu8MJC2aifRiC80pmwHyleZpij13_5HQpRkFuJc7KkHBE1p3Gk',
//   keys:
//    { p256dh: 'BHi8yagXYq0CS2/0QI9wEe2FSeyTPY2pcwReyZYXr0iGvo0ib6aCxhUuAB5oWDbo/BYqztLGjqCGlqi3Xgiy7FI=',
//      auth: 'k+wv8CW3J+hxe6/nEj1rtw==' } },
//      { endpoint: 'https://updates.push.services.mozilla.com/wpush/v2/gAAAAABariqrD_7x3OEEgguVw6UGVK7ExCqvKRBraIb4IRru1XWH5IMX4MicY4wj8tkW7cWYwgPvmDo1wxZNUtnnmD2txY6zWx2YqRVZ1_0TZLn0vstQ7cHNff9sOBJjL1RYMkdZB_-TUi-WUT-78vQfEeeV4TAmQAVuIqP7gdHPgtQfV7E9b1k',
//   keys:
//    { p256dh: 'BCCkCTM2uxCC8BCb2p+vNfwjkzuuzYnDD8hCulW4zSH7QdKQAGIU1Np+dwPmRtsOnquJ9nBJiOnpElMi4m5DAlA=',
//      auth: 'wJaNBrueu0F+4Z6OEy736g==' } }

     
let subscribers = [];
//demoserver
let VAPID_SUBJECT = 'mailto:thihara@favoritemedium.com';
let VAPID_PUBLIC_KEY = 'BCLcULyGxq_ZTk8SYqYJkD-D-svy8WbC8hRJTDDZJpFKnl6WaT5hklpnV6CGK0RJsO0-wpioSAG0Dp9d8mVBdbw';
let VAPID_PRIVATE_KEY = '_xKqyzMFZn-ekBu4c81ZOJlq6OOWAJIs26kUFGj9IFY';

//Auth secret used to authentication notification requests.
let AUTH_SECRET = 'qwertyuiop';

if (!VAPID_SUBJECT) {
    return console.error('VAPID_SUBJECT environment variable not found.')
} else if (!VAPID_PUBLIC_KEY) {
    return console.error('VAPID_PUBLIC_KEY environment variable not found.')
} else if (!VAPID_PRIVATE_KEY) {
    return console.error('VAPID_PRIVATE_KEY environment variable not found.')
} /*else if (!AUTH_SECRET) {
    return console.error('AUTH_SECRET environment variable not found.')
}
*/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

webPush.setVapidDetails(
    VAPID_SUBJECT,
    VAPID_PUBLIC_KEY,
    VAPID_PRIVATE_KEY
);

app.use(express.static('static'));

app.get('/status', function (req, res) {
    res.send('Server Running!')
});

app.get('/notify/all', function (req, res) {
    /*if(req.get('auth-secret') != AUTH_SECRET) {
        console.log("Missing or incorrect auth-secret header. Rejecting request.");
        return res.sendStatus(401);
    }
    */
    let message = req.query.message || `Willy Wonka's chocolate is the best!`;
    let clickTarget = req.query.clickTarget || `http://www.favoritemedium.com`;
    let title = req.query.title || `Push notification received!`;

    subscribers.forEach(pushSubscription => {
        //Can be anything you want. No specific structure necessary.
        let payload = JSON.stringify({message : message, clickTarget: clickTarget, title: title});

        webPush.sendNotification(pushSubscription, payload, {}).then((response) =>{
            console.log("Status : "+util.inspect(response.statusCode));
            console.log("Headers : "+JSON.stringify(response.headers));
            console.log("Body : "+JSON.stringify(response.body));
        }).catch((error) =>{
            console.log("Status : "+util.inspect(error.statusCode));
            console.log("Headers : "+JSON.stringify(error.headers));
            console.log("Body : "+JSON.stringify(error.body));
        });
    });

    res.send('Notification sent!');
});

app.post('/subscribe', function (req, res) {
    let endpoint = req.body['notificationEndPoint'];
    let publicKey = req.body['publicKey'];
    let auth = req.body['auth'];
    
    let pushSubscription = {
        endpoint: endpoint,
        keys: {
            p256dh: publicKey,
            auth: auth
        }
    };
    console.log(pushSubscription);
    subscribers.push(pushSubscription);

    res.send('Subscription accepted!');
});

app.post('/unsubscribe', function (req, res) {
    let endpoint = req.body['notificationEndPoint'];
    
    subscribers = subscribers.filter(subscriber => { endpoint == subscriber.endpoint });

    res.send('Subscription removed!');
});

let PORT = process.env.PORT || 5000;
app.listen(PORT, function () {
    console.log(`push_server listening on port ${PORT}!`)
});
