/**
 * Created by thihara on 8/29/16.
 * 
 * The service worker for displaying push notifications.
 */

function messageToClient(client, data) {
  return new Promise(function(resolve, reject) {
    const channel = new MessageChannel();

    channel.port1.onmessage = function(event){
      if (event.data.error) {
        reject(event.data.error);
      } else {
        resolve(event.data);
      }
    };

    client.postMessage(JSON.stringify(data), [channel.port2]);
  });
}
self.addEventListener('install', function(event) {
    event.waitUntil(self.skipWaiting()); // Activate worker immediately
});

self.addEventListener('activate', function(event) {
    event.waitUntil(self.clients.claim()); // Become available to all pages
});
self.addEventListener('push', function(event) {
    if (!(self.Notification && self.Notification.permission === 'granted')) {
        return;
    }

    var data = {};
    if (event.data) {
        data = event.data.json();
    }
    var title = data.title;
    var message = data.message;
    var icon = "img/FM_logo_2013.png";

console.log('[Service Worker] Notification click Received.');

    if (!event.clientId) return;

    // Get the client.
    const client = clients.get(event.clientId);
    // Exit early if we don't get the client.
    // Eg, if it closed.
    if (!client) return;

    // Send a message to the client.
    client.postMessage({
      msg: "Hey I just got a fetch from you!",
      url: event.request.url
    });


    self.clickTarget = data.clickTarget;

    event.waitUntil(self.registration.showNotification(title, {
        body: message,
        tag: 'push-demo',
        icon: icon,
        badge: icon
      }))
    // .then(function() {
        
    //     clients.matchAll({type: 'window'}).then(function (clientList) {
    //       if (clientList.length > 0) {
    //         messageToClient(clientList[0], {
    //           message: "Hello World !" // suppose it is: "Hello World !"
    //         });
    //       }
    //     });
    // }));    
// event.waitUntil(self.registration.showNotification(title, {
    //     body: message,
    //     tag: 'push-demo',
    //     icon: icon,
    //     badge: icon
    // })).then(function() {
    //     clients.matchAll({type: 'window'}).then(function (clientList) {
    //       if (clientList.length > 0) {
    //         messageToClient(clientList[0], {
    //           message: self.pushData.body // suppose it is: "Hello World !"
    //         });
    //       }
    // });;
});

self.addEventListener('notificationclick', function(event) {
    console.log('[Service Worker] Notification click Received1.');

    event.notification.close();

    if(clients.openWindow){
        event.waitUntil(clients.openWindow(self.clickTarget));
    }
});
 console.log('[Service Worker] Notification click Received1.');
self.addEventListener('message', function(event){
    console.log("SW Received Message: " + event.data);
});