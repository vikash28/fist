var FCM = require('fcm-push');

var serverKey = 'AAAADa28jk0:APA91bHK9cFfJoyxcM960_5N7FVRspebI9btq2r6kOiNrr6akX-3dD22kw7-Z5DYKfexCrMgpqi0ASo1QA8RPIkGS_SoW9N_TOBLjeFvuLST2YNgAb4bOVsro9_CcfKQzOsklURrIPrj';
var fcm = new FCM(serverKey);

var message = {
    to: 'fgbqVM7WJgU:APA91bGqvgPIVAKws-zPIx_l0Qk5zlQgZJHBAWW4K-x_E8Zys8yFDRC96VumThtDvRBxG9HsTWHazJBpPPFi81dN_-hxOxKc-zNiEafEeV6B_Rtzllp5SCnfYiowi4u5KY2TML1C3xvo', // required fill with device token or topics
    collapse_key: 'do_not_collapse', 
    data: {
        your_custom_data_key: 'your_custom_data_value'
    },
    notification: {
        title: 'Title of your push notification',
        body: 'Body of your push notification'
    }
};

//callback style
fcm.send(message, function(err, response){
    if (err) {
        console.log("Something has gone wrong!");
    } else {
        console.log("Successfully sent with response: ", response);
    }
});

//promise style
fcm.send(message)
    .then(function(response){
        console.log("Successfully sent with response: ", response);
    })
    .catch(function(err){
        console.log("Something has gone wrong!");
        console.error(err);
    })